import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginComponent } from '../pages/login/login.component';
import { NetworkInterface } from '@ionic-native/network-interface';
import { Geolocation } from '@ionic-native/geolocation';
import { LogoutComponent } from '../pages/logout/logout.component';
import { ToDOListPage } from '../pages/todolist/todolist';
import { SettingComponent } from '../pages/setting/setting.component';
import { ContactusComponent } from '../pages/contactus/contactus.component';
import { ContactlistComponent } from '../pages/contactlist/contactlist.component';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{ icon: string, title: string, component: any }>;
  latitude;
  longitude;
  username = '';
  useremail = '';
  userAvatar = 'assets/imgs/profile-photo.jpg';
  currentPageTitle = 'Home';

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private networkinterface: NetworkInterface, private geolocation: Geolocation) {
    this.initializeApp();
    this.username = localStorage.getItem('firstname') + ' '+localStorage.getItem('lastname');
    this.useremail = localStorage.getItem('email');
    this.userAvatar = localStorage.getItem('userPhoto');

    // used for an example of ngFor and navigation
    this.pages = [
      { icon: 'ios-home', title: 'Home', component: HomePage },
      { icon: 'ios-settings', title: 'Setting', component: SettingComponent },
      { icon: 'ios-mail', title: 'Contact Us', component: ContactusComponent },
      { icon: 'ios-list-box', title: ' How TO-DO', component: ToDOListPage },
      { icon: 'ios-contact', title: 'Contactlist', component: ContactlistComponent },
      { icon: 'ios-log-out', title: 'Logout', component: LogoutComponent }
    ];
    if (!localStorage.getItem('userID')) {
      this.rootPage = LoginComponent
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // get ip address
      this.networkinterface.getWiFiIPAddress()
        .then(address =>  //console.log('IP:' + address.ip + 'subnet:' + address.subnet)
          alert('Ip:' + address.ip + '\nsubnet:' + address.subnet))
        .catch(error => console.error('Unable to get ip address'));
    });


    //get current location
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude
      this.longitude = resp.coords.longitude
      localStorage.setItem('latitude', this.latitude);
      localStorage.setItem('longitude', this.longitude);
    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
    this.currentPageTitle = page.title;
  }
}
