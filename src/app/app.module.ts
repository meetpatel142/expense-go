import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginComponent } from '../pages/login/login.component';
import { SignUpComponent } from '../pages/sign-up/sign-up.component';
import { Dataservice } from '../provider/dataservice';
import { HttpModule } from '@angular/http';
import { NetworkInterface } from '@ionic-native/network-interface';
import { Geolocation } from '@ionic-native/geolocation';
import { LogoutComponent } from '../pages/logout/logout.component';
import { ToDOListPage } from '../pages/todolist/todolist';
import { CurrencylistComponent } from '../pages/currencylist/currencylist.component';
import { UserprofileComponent } from '../pages/userprofile/userprofile.component';
import { CountrycodelistComponent } from '../pages/countrycodelist/countrycodelist.component';
import { ForgotpasswordComponent } from '../pages/forgotpassword/forgotpassword.component';
import { ContactlistComponent } from '../pages/contactlist/contactlist.component';
import { CreategroupComponent } from '../pages/creategroup/creategroup.component';
import { BillAddComponent } from '../pages/bill-add/bill-add.component';
import { BillListComponent } from '../pages/bill-list/bill-list.component';
import { SettingComponent } from '../pages/setting/setting.component';
import { ContactusComponent } from '../pages/contactus/contactus.component';
import { CreateFriendComponent } from '../pages/create-friend/create-friend.component';
import { FriendsListComponent } from '../pages/friendslist/friendslist.component';
import { FriendDetailComponent } from '../pages/friend-detail/friend-detail.component';
import { GroupDetailComponent } from '../pages/group-detail/group-detail.component';
import { Contacts } from '@ionic-native/contacts';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginComponent,
    ToDOListPage,
    SignUpComponent,
    LogoutComponent,
    CurrencylistComponent,
    UserprofileComponent,
    CountrycodelistComponent,
    ContactlistComponent,
    ForgotpasswordComponent,
    CreategroupComponent,
    BillAddComponent,
    BillListComponent,
    SettingComponent,
    ContactusComponent,
    FriendsListComponent,
    CreateFriendComponent,
    FriendDetailComponent,
    GroupDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, { tabsPlacement: 'bottom' }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ToDOListPage,
    LoginComponent,
    SignUpComponent,
    LogoutComponent,
    CurrencylistComponent,
    UserprofileComponent,
    CountrycodelistComponent,
    ContactlistComponent,
    ForgotpasswordComponent,
    CreategroupComponent,
    BillAddComponent,
    BillListComponent,
    SettingComponent,
    ContactusComponent,
    FriendsListComponent,
    CreateFriendComponent,
    FriendDetailComponent,
    GroupDetailComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Dataservice,
    NetworkInterface,
    Geolocation,
    Contacts,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
