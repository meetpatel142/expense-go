import { Component } from '@angular/core';
import { ToastController, AlertController, LoadingController } from 'ionic-angular';
import { Dataservice } from '../../provider/dataservice';

@Component({
  selector: 'bill-add',
  templateUrl: 'bill-add.component.html',
  //  styleUrls: ['bill-add.component.scss']
})
export class BillAddComponent {

  description = "";
  amount = "";
  loading: any;
  constructor(public toastctrl: ToastController, public loadingctrl: LoadingController, public alertctrl: AlertController, public dataservice: Dataservice) {

  }

  addbill() {
    if (!this.description) {
      let toast = this.toastctrl.create({
        message: 'Description is required',
        duration: 3000,
        position: 'bottom'
      })
      toast.present();
    }
    else if (!this.amount) {
      let toast = this.toastctrl.create({
        message: 'Amount is required',
        duration: 3000,
        position: 'bottom'
      })
      toast.present();
    }
    else {
      //  this.showloader();
      const data = { 'userId': localStorage.getItem('userID'), 'description': this.description, 'cost': this.amount }

      /*  this.dataservice.addbill(data).subscribe((res:any)=> {
          this.loading.dismiss();
          if(res.code == 200){
              let alert = this.alertctrl.create({
                message: res.message,
                buttons: ['Ok']
              });
              alert.present();
          }
          else{
            let alert = this.alertctrl.create({
              message: res.message,
              buttons: ['Ok']
            });
            alert.present();
          }
        }) */

    }
  }

  showloader() {
    this.loading = this.loadingctrl.create({
      content: 'Please wait..'
    })
    this.loading.present;
  }
}
