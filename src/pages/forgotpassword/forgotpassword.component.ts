import { Component } from '@angular/core';
import { Dataservice } from '../../provider/dataservice';
import { AlertController } from 'ionic-angular';

@Component({
    selector: 'forgotpassword',
    templateUrl: 'forgotpassword.component.html',
   // styleUrls: ['forgotpassword.component.scss']
})
export class ForgotpasswordComponent {

    email = "";
    constructor(public dataservice: Dataservice , public alertctrl: AlertController){}

    forgotpassword(){
        const data ={'email': this.email};
        this.dataservice.forgotpassword(data).subscribe((res:any)=>{
            if(res.code == 200){
              let alert =  this.alertctrl.create({
                    subTitle:res.message,
                    buttons: ['Ok']
                });
                alert.present();
            }
            else{
                let alert =  this.alertctrl.create({
                    subTitle:res.message,
                    buttons: ['Ok']
                });
                alert.present();
            }
        })
    }

}
