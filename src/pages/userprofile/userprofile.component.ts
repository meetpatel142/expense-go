import { Component, OnInit } from '@angular/core';
import { Dataservice } from '../../provider/dataservice';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { CountrycodelistComponent } from '../countrycodelist/countrycodelist.component';
import { HomePage } from '../home/home';

@Component({
  selector: 'userprofile',
  templateUrl: 'userprofile.component.html',
})
export class UserprofileComponent implements OnInit {

  form: FormGroup;
  loading;
  fileExt = "";
  profileImage = "";
  userid;
  userdetail;
  mobile_code;
  firstname: FormControl;
  lastname: FormControl;
  mobileno: FormControl;
  constructor(public dataservice: Dataservice, public alertCtrl: AlertController, public navctrl: NavController, public navparams: NavParams) {
    this.userid = localStorage.getItem('userID');
    if (this.navparams.get('mobile_code')) {
      this.mobile_code = this.navparams.get('mobile_code');
    } 
    this.userdetail = [];
  }

  ngOnInit() {
    this.createformcontrol();
    this.createformgroup();
    this.getuserinfo();
  }

  createformcontrol() {
    this.firstname = new FormControl("", Validators.required),
      this.lastname = new FormControl("", Validators.required),
      this.mobileno = new FormControl("")
  }
  createformgroup() {
    this.form = new FormGroup({
      firstname: this.firstname,
      lastname: this.lastname,
      mobileno: this.mobileno
    })
  }

  getuserinfo() {
    this.dataservice.getuserinfo(this.userid).subscribe((res: any) => {
      this.userdetail = res.data;
      this.form.controls['firstname'].setValue(this.userdetail[0].firstname);
      this.form.controls['lastname'].setValue(this.userdetail[0].lastname);
      this.form.controls['mobileno'].setValue(this.userdetail[0].mobile);
      this.mobile_code = this.userdetail[0].mobile_code;
      this.profileImage = this.userdetail[0].file_path;
    });
  }

  updateprofile() {
    if (this.form.valid) {
      const data = {
        'id': this.userid,
        'firstname': this.form.value.firstname,
        'lastname': this.form.value.lastname,
        'mobile': this.form.value.mobileno,
        'mobile_code': this.mobile_code,
        'url': ''
      }
      if (this.fileExt) {
        let imgdata = this.profileImage.split(';base64,');
        data.url = imgdata[1];
      }
      this.dataservice.updateprofile(data).subscribe((res: any) => {
        if (res.code == 200) {
          let alert = this.alertCtrl.create({
            subTitle: res.message,
            buttons: ['Ok']
          });
          alert.present();
          this.navctrl.setRoot(HomePage);
        }
      });
    }
  }

  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {

      var ext = file.name.split('.').pop();
      this.fileExt = ext;
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
  }
  _handleReaderLoaded(readerEvt) {
    this.profileImage = readerEvt.target.result;
  }

  getcountrycode() {
    this.navctrl.push(CountrycodelistComponent);
  }
}
