import { Component } from '@angular/core';
import { Dataservice } from '../../provider/dataservice';
import { AlertController, NavController, Button } from 'ionic-angular';
import { FriendsListComponent } from '../friendslist/friendslist.component';

@Component({
    selector: 'create-friend',
    templateUrl: 'create-friend.component.html',
   // styleUrls: ['create-friend.component.scss']
})
export class CreateFriendComponent {

    userID;
    contact = [];
    constructor( public dataservice: Dataservice , public alertCtrl: AlertController,public navctrl: NavController){
        this.userID = localStorage.getItem('userID');
        this.contact= [{
            'name': 'demo1',
            'email': 'demo1@weavolve.com',
            'mobile':'6547891123',
            'mobile_code':'+91'
        },{
            'name':'test1',
            'email':'',
            'mobile':'3456789021',
            'mobile_code': '+91'
        }]
    }

    save(){
        const data = {'user_id': this.userID,'user': this.contact}
        this.dataservice.createfriend(data).subscribe((res: any)=>{
            if(res.code == 200){
                this.navctrl.setRoot(FriendsListComponent);
            }
            else{
                let alert = this.alertCtrl.create({
                    message:res.message,
                    buttons: ['Ok']
                })
                alert.present();
            }
        })
    }

}
