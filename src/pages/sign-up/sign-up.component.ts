import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Dataservice } from '../../provider/dataservice';
import { AlertController, LoadingController, NavController, NavParams } from 'ionic-angular';
import { ToDOListPage } from '../todolist/todolist';
import { CurrencylistComponent } from '../currencylist/currencylist.component';
import { CountrycodelistComponent } from '../countrycodelist/countrycodelist.component';

@Component({
  selector: 'sign-up',
  templateUrl: 'sign-up.component.html',
  //  styleUrls: ['sign-up.component.scss']
})
export class SignUpComponent {

  form;
  loading: any;
  name;
  symbol;
  mobile_code;
  constructor(public dataservice: Dataservice, public alertCtrl: AlertController, public loadingctrl: LoadingController, public navctrl: NavController, public navparams: NavParams) {
    this.form = new FormGroup({
      email: new FormControl("", [Validators.required,
      Validators.pattern('^[a-zA-Z][a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]{3,}([.][a-zA-Z0-9-]{2,})+$')
      ]),
      password: new FormControl("", [Validators.required,
      Validators.minLength(6)]),
      firstname: new FormControl("", Validators.required),
      lastname: new FormControl("", Validators.required),
      mobileno: new FormControl(""),
      mobile_code: new FormControl("")
    });
    if (!this.navparams.get('symbol') && !this.navparams.get('mobile_code')) {
      this.name = 'Rupees',
      this.symbol = 'Rs',
      this.mobile_code = "+91"
    }
    else {
      this.name = this.navparams.get('name');
      this.symbol = this.navparams.get('symbol');
      this.mobile_code = localStorage.getItem('mobile_code');
    }
    if( this.navparams.get('mobile_code') && !this.navparams.get('symbol')){
      this.mobile_code = this.navparams.get('mobile_code');
      this.symbol= localStorage.getItem('symbol');
    }
    if(!this.navparams.get('mobile_code') && !this.navparams.get('symbol')){
       localStorage.setItem('mobile_code', '+91') ;
      localStorage.setItem('symbol','₹');
      this.mobile_code = localStorage.getItem('mobile_code');
      this.symbol= localStorage.getItem('symbol');
    } 
  }
 

  signup() {
    if (this.form.valid) {
      this.showloader();
      const data = { 'firstname': this.form.value.firstname, 'lastname': this.form.value.lastname, 'email': this.form.value.email, 'password': this.form.value.password, 'mobile': this.form.value.mobileno , 'mobile_code':this.mobile_code , 'type': 'User' };

      this.dataservice.register(data).subscribe((res: any) => {
        this.loading.dismiss();
        if (res.code == 200) {
          this.navctrl.setRoot(ToDOListPage);
        }
        else {
          let alert = this.alertCtrl.create({
            subTitle: res.message,
            buttons: ['Ok']
          })
          alert.present();
        }
      });
    }
    else {
      let alert = this.alertCtrl.create({
        subTitle: 'Something goes wrong.Please try again',
        buttons: ['Ok']
      })
      alert.present();
    }
  }

  showloader() {
    this.loading = this.loadingctrl.create({
      content: 'Please wait..'
    });
    this.loading.present();
  }

  getcountrycode() {
    this.navctrl.push(CountrycodelistComponent);
  }

  getcurrency() {
    this.navctrl.push(CurrencylistComponent);
  }

}
