import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BillAddComponent } from '../bill-add/bill-add.component';
import { FriendsListComponent } from '../friendslist/friendslist.component';
import { Dataservice } from '../../provider/dataservice';
import { GroupDetailComponent } from '../group-detail/group-detail.component';
import { CreategroupComponent } from '../creategroup/creategroup.component';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  showFabButton = false;
  grouplist : any;
  userID;
  constructor(public navCtrl: NavController, public dataservice: Dataservice, public navctrl: NavController) {
    this.userID = localStorage.getItem('userID');
  }
  ngOnInit() {
    const data = { 'user_id': this.userID };
    this.dataservice.getgrouplisting(data).subscribe((res: any) => {
      if (res.code == 200) {
        this.grouplist = res.data;
        
      }
    });
  }

  getgroupdetail(id) {
    this.navctrl.push(GroupDetailComponent, { 'group_id': id });
  }

  addgroup() {
    this.navctrl.push(CreategroupComponent);
  }

  createnewbill() {
    this.navCtrl.push(BillAddComponent);
  }
  friendpage() {
    this.navCtrl.setRoot(FriendsListComponent)
  }
}
