import { Component, OnInit } from '@angular/core';
import { Dataservice } from '../../provider/dataservice';
import { NavController } from 'ionic-angular';
import { SignUpComponent } from '../sign-up/sign-up.component';

@Component({
    selector: 'currencylist',
    templateUrl: 'currencylist.component.html',
   // styleUrls: ['currencylist.component.scss']
})
export class CurrencylistComponent implements OnInit {

    currencylist;
    constructor(public dataservice : Dataservice , public navCtrl: NavController){

    }

    ngOnInit(){
        this.getcurrency();
    }
     getcurrency(){
        this.dataservice.getcurrencylist().subscribe((res:any) => {
            this.currencylist = res.data;
            console.log(this.currencylist);
        });
    }

    getselecteditem(item){
      localStorage.setItem('symbol',item.symbol);
      this.navCtrl.setRoot(SignUpComponent,{'name': item.name , 'symbol':item.symbol})
    }
}

