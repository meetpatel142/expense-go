import { Component } from '@angular/core';
import { Dataservice } from '../../provider/dataservice';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { SignUpComponent } from '../sign-up/sign-up.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { ForgotpasswordComponent } from '../forgotpassword/forgotpassword.component';

@Component({
    selector: 'login',
    templateUrl: 'login.component.html',
    // styleUrls: ['login.component.scss']
})
export class LoginComponent {
    form;
    loading: any;
    constructor(public dataservice: Dataservice, public navctrl: NavController, public alertctrl: AlertController, public loadingctrl: LoadingController) {
        this.form = new FormGroup({
            email: new FormControl("", [
                Validators.pattern('^[a-zA-Z][a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]{3,}([.][a-zA-Z0-9-]{2,})+$')
            ]),
            password: new FormControl("", [Validators.required,
            Validators.minLength(6)])
        });
    }

    login() {
        if (this.form.valid) {
            const data = { 'email': this.form.value.email, 'password': this.form.value.password }
            this.showloader();
            this.dataservice.login(data).subscribe((res: any) => {
                this.loading.dismiss();
                console.log(res);
                if (res.code == 200) {
                    localStorage.setItem('userID', res.data.id);
                    localStorage.setItem('firstname', res.data.firstname);
                    localStorage.setItem('lastname', res.data.lastname);
                    localStorage.setItem('email', res.data.email);
                    localStorage.setItem('mobileno',res.data.mobile);
                    localStorage.setItem('userPhoto',res.data.photo);
                    this.navctrl.setRoot(HomePage);
                }
                else {
                    let alert = this.alertctrl.create({
                        subTitle: res.message,
                        buttons: ['Ok']
                    })
                    alert.present();
                }
            });
        }
    }

    signup() {
        this.navctrl.setRoot(SignUpComponent);
    }

    showloader() {
        this.loading = this.loadingctrl.create({
            content: 'Please wait ...'
        })
        this.loading.present();
    }

    forgotpassword(){
        this.navctrl.push(ForgotpasswordComponent);
    }
}
