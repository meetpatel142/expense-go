import { Component, OnInit } from '@angular/core';
import { NavParams, NavController, AlertController } from 'ionic-angular';
import { Dataservice } from '../../provider/dataservice';
import { FriendsListComponent } from '../friendslist/friendslist.component';

@Component({
    selector: 'friend-detail',
    templateUrl: 'friend-detail.component.html',
   // styleUrls: ['friend-detail.component.scss']
})
export class FriendDetailComponent implements OnInit {

    user_id;
    friend_id;
    detaildata =[];
    constructor(public navparams: NavParams,public alertctrl: AlertController,public dataservice: Dataservice, public navctrl: NavController){
            this.user_id = localStorage.getItem('userID');
            this.friend_id = this.navparams.get('friend_id');
    }

    ngOnInit(){
        const data ={ 'user_id': this.user_id, 'friend_id': this.friend_id};
        this.dataservice.frienddetail(data).subscribe((res:any)=>{
            this.detaildata= res.data;
        })
    }

    deletefriend(){
        const data ={ 'user_id': this.user_id, 'friend_id': this.friend_id};
         this.dataservice.deletefriend(data).subscribe((res:any)=>{
             if(res.code == 200){
                 this.navctrl.push(FriendsListComponent);
             }
             else{
                let alert = this.alertctrl.create({
                    message: res.message,
                    buttons: ['Ok']
                })
                alert.present();
             }
         })
    }

}
