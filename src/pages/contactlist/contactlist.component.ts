import { Component, OnInit } from '@angular/core';
import { Contacts } from '@ionic-native/contacts';
import { NavController } from 'ionic-angular';
import { CreategroupComponent } from '../creategroup/creategroup.component';

@Component({
    selector: 'contactlist',
    templateUrl: 'contactlist.component.html',
    // styleUrls: ['contactlist.component.scss']
})
export class ContactlistComponent implements OnInit {
    filter;
    contactfound: any = [];
    showlist = false;
    newdata: any;
    searchterm = '';
    subitem: string;
    searchvalue: string;
    constructor(public contacts: Contacts, public navctrl: NavController) {
    }
    ngOnInit() {
        this.getcontactlist();
    }

    getcontactlist() {
        this.filter = ["displayName", 'name', 'phoneNumbers'];
        this.contacts.find(this.filter, { filter: '', multiple: true }).then((allcontacts) => {
            this.contactfound = allcontacts
        });
    }

    findcontact(event) {
        this.searchvalue = event.target.value;
        if (this.searchvalue && this.searchvalue.trim() != '') {
              this.contactfound = this.contactfound.filter(item =>{
                   this.subitem = item.displayName;
                   if(this.subitem !=null){
                      return (this.subitem.toLowerCase().indexOf(this.searchvalue.toLowerCase()) > -1);
                  }
             });
             this.showlist =true;         
          }
           else {
              this.showlist =false;
          } 
    }

    gotogrouppage(item) {
        let mobilenos = [];
        for (let i = 0; i < item.phoneNumbers.length; i++) {
            if (item.phoneNumbers[i].type == 'mobile') {
                mobilenos.push(item.phoneNumbers[i].value);
            }
        }
        this.newdata = { 'name': item.displayName, 'mobileno': mobilenos, 'mobile_code': '+91', 'email': '' };
        this.navctrl.setRoot(CreategroupComponent, { 'contact': this.newdata });
    }
}
