import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-todolist',
  templateUrl: 'todolist.html'
})
export class ToDOListPage {
 

  constructor(public navCtrl: NavController) {
  
  }

  getstarted(){
    this.navCtrl.setRoot(HomePage);
  }
}
