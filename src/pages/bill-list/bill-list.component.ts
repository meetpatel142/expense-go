import { Component, OnInit } from '@angular/core';
import { Dataservice } from '../../provider/dataservice';

@Component({
    selector: 'bill-list',
    templateUrl: 'bill-list.component.html',
    // styleUrls: ['bill-list.component.scss']
})
export class BillListComponent implements OnInit {

    userID;
    listdata = [];
    constructor(public dataservice: Dataservice) {
        this.userID = localStorage.getItem('userID');
        this.listdata = [{
            'id': '1',
            'name': 'Bansari',
            'currency': '₹',
            'amount': '40',
            'image':'assets/imgs/profile-photo.jpg'
        },
        {
            'id': '2',
            'name': 'Shivani',
            'currency': '₹',
            'amount': '30',
            'image':'assets/imgs/profile-photo.jpg'
        }, 
        {
            'id': '3',
            'name': 'Test',
            'currency': '₹',
            'amount': '40',
            'image':'assets/imgs/profile-photo.jpg'
         },
        {
            'id':'4',
            'name': 'Demo',
            'currency': '₹',
            'amount': '200',
            'image':'assets/imgs/profile-photo.jpg'
        }]
    }
    ngOnInit() {
        // this.getbillListing();
    }
    getbillListing() {
        this.dataservice.getbillListing(this.userID).subscribe((res: any) => {
            this.listdata = res.data;
        });
    }
}
