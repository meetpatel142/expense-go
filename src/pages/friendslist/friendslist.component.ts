import { Component, OnInit } from '@angular/core';
import { Dataservice } from '../../provider/dataservice';
import { NavController } from 'ionic-angular';
import { CreateFriendComponent } from '../create-friend/create-friend.component';
import { FriendDetailComponent } from '../friend-detail/friend-detail.component';
import { HomePage } from '../home/home';

@Component({
  selector: 'friends',
  templateUrl: 'friendslist.component.html',
  // styleUrls: ['friends.component.scss']
})
export class FriendsListComponent implements OnInit {

  friendlist = [];
  userID;
  constructor(public dataservice: Dataservice, public navctrl: NavController) {
    this.userID = localStorage.getItem('userID');
  }

  ngOnInit() {
    this.getfriendlist();
  }

  getfriendlist() {
    this.dataservice.getfriendlisting(this.userID).subscribe((res: any) => {
      console.log(res);
      if (res.code == 200) {
        this.friendlist = res.data;
        console.log( this.friendlist);
      }
    })
  }
  addfriend() {
    this.navctrl.push(CreateFriendComponent);
  }
  getfrienddetail(id) {
    this.navctrl.push(FriendDetailComponent, { 'friend_id': id });
  }

  grouppage() {
    this.navctrl.setRoot(HomePage);
  }
}
