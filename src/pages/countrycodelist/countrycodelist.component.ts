import { Component, OnInit } from '@angular/core';
import { Dataservice } from '../../provider/dataservice';
import { NavController } from 'ionic-angular';
import { SignUpComponent } from '../sign-up/sign-up.component';

@Component({
    selector: 'countrycodelist',
    templateUrl: 'countrycodelist.component.html',
    //  styleUrls: ['countrycodelist.component.scss']
})
export class CountrycodelistComponent implements OnInit {

    countrycode;
    constructor(public dataservice: Dataservice, public navctrl: NavController) {

    }

    ngOnInit() {
        this.getcountrycode();
    }
    getcountrycode() {
        this.dataservice.getcountrycode().subscribe((res: any) => {
            this.countrycode = res.data;
        })
    }

    getselecteditem(item) {
        localStorage.setItem('mobile_code', item.mobile_country_code);
        this.navctrl.setRoot(SignUpComponent, { 'mobile_code': item.mobile_country_code })
    }
}
