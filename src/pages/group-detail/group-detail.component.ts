import { Component, OnInit } from '@angular/core';
import { NavParams, AlertController, NavController } from 'ionic-angular';
import { Dataservice } from '../../provider/dataservice';
import { HomePage } from '../home/home';

@Component({
  selector: 'group-detail',
  templateUrl: 'group-detail.component.html',
  //  styleUrls: ['group-detail.component.scss']
})
export class GroupDetailComponent implements OnInit {

  group_id;
  groupdetail;
  constructor(public navparams: NavParams, public navctrl: NavController, public alertctrl: AlertController, public dataservice: Dataservice) {
    this.group_id = this.navparams.get('group_id');
  }

  ngOnInit() {
    this.getgroupdetail();
  }

  getgroupdetail() {
    this.dataservice.getgroupdetail(this.group_id).subscribe((res: any) => {
      this.groupdetail = res.data;
    })
  }

  deletegroup() {
    this.dataservice.deletegroup(this.group_id).subscribe((res: any) => {
      if (res.code == 200) {
        this.navctrl.push(HomePage)
      }
      else {
        let alert = this.alertctrl.create({
          message: res.message,
          buttons: ['Ok']
        })
      }
    })
  }

  deletemember(id) {
    const data = { 'group_id': this.group_id, 'user_id': id };
    this.dataservice.deleteUserFromGroup(data).subscribe((res: any) => {
      if (res.code == 200) {
        let alert = this.alertctrl.create({
          message: res.data,
          buttons: ['Ok']
        })
        alert.present();
        this.getgroupdetail();
      }
    })
  }

}
