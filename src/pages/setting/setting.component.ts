import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserprofileComponent } from '../userprofile/userprofile.component';

@Component({
    selector: 'setting',
    templateUrl: 'setting.component.html',
  //  styleUrls: ['setting.component.scss']
})
export class SettingComponent {
  
  constructor(public navctrl: NavController){}
  accountsetting(){
    this.navctrl.push(UserprofileComponent);
  }

}
