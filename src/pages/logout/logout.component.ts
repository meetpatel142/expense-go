import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginComponent } from '../login/login.component';

@Component({
    selector: 'logout',
    templateUrl: 'logout.component.html',
    // styleUrls: ['logout.component.scss']
})
export class LogoutComponent {

    constructor(public navctrl: NavController) {
        localStorage.clear();
        this.navctrl.setRoot(LoginComponent)
    }

}
