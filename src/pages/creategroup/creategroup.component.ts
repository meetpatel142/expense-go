import { Component } from '@angular/core';
import { Dataservice } from '../../provider/dataservice';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ContactlistComponent } from '../contactlist/contactlist.component';
import { HomePage } from '../home/home';

@Component({
    selector: 'creategroup',
    templateUrl: 'creategroup.component.html',
    // styleUrls: ['creategroup.component.scss']
})
export class CreategroupComponent {

    groupname;
    userId;
    shouldShowCancel = true;
    contact: any = [];

    constructor(public dataservice: Dataservice, public alertctrl: AlertController, public navctrl: NavController, public navparams: NavParams) {
        this.userId = localStorage.getItem('userID');
        if (this.navparams.get('contact')) {
            this.contact.push(this.navparams.get('contact'));
            console.log('contactdata');
            console.log(this.contact);
        }
        else {
            this.contact = [];
        }



        /*   this.contact=[{
               'name': 'Test2',
               'email':'test2@weavolve.com',
               'mobile': '2341567890',
               'mobile_code': '+91'
           },
           {
               'name': 'Test3',
               'email':'',
               'mobile': '5678432190',
               'mobile_code': '+91'
           },
           {
               'name': 'Test4',
               'email':'test4@weavolve.com',
               'mobile': '',
               'mobile_code': ''
           }] */
    }

    save() {
        const data = { 'user_id': this.userId, 'group_name': this.groupname, 'group_type': 'User', 'user': this.contact };
        this.dataservice.creategroup(data).subscribe((res: any) => {
            if (res.code == 200) {
                this.navctrl.setRoot(HomePage);
            }
            else {
                let alert = this.alertctrl.create({
                    message: res.message,
                    buttons: ['Ok']
                })
                alert.present();
            }
        });
    }

    addmember() {
        this.navctrl.setRoot(ContactlistComponent);
    }

}
