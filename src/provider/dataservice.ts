import { Injectable } from "@angular/core";
import { Headers, Http } from "@angular/http";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/Observable";

@Injectable()

export class Dataservice {

   // apiUrl = 'http://expensego.com/index.php/api/';
      apiUrl = 'http://111.125.194.139/index.php/api/';
  // apiUrl = "http://expensego.weavolve.com/index.php/api/";
    constructor(public http: Http) { }

    login(data): Observable<ResponseModel> {
        let headers = new Headers();
        headers.append('Content-Type', 'appliction/json');
        return this.http.post(this.apiUrl + 'login', JSON.stringify(data), { headers: headers })
            .map(res => <ResponseModel>res.json());
    }
    register(data): Observable<ResponseModel> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.apiUrl + 'register', JSON.stringify(data), { headers: headers })
            .map(res => <ResponseModel>res.json());
    }
    getcountrycode(): Observable<ResponseModel> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get(this.apiUrl + 'get_country_code', { headers: headers })
            .map(res => <ResponseModel>res.json());
    }
    getcurrencylist(): Observable<ResponseModel> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get(this.apiUrl + 'get_currency', { headers: headers })
            .map(res => <ResponseModel>res.json());
    }
    getuserinfo(userId): Observable<ResponseModel>{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.get(this.apiUrl+ 'userInfo/'+ userId, {headers: headers})
        .map(res => <ResponseModel> res.json());
    }
    updateprofile(data): Observable<ResponseModel>{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.post(this.apiUrl+ 'profile_update', JSON.stringify(data), { headers: headers})
        .map(res => <ResponseModel> res.json());
    }
    forgotpassword(data):Observable<ResponseModel>{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.post(this.apiUrl + 'forgot_pass' , JSON.stringify(data),{ headers: headers})
        .map(res => <ResponseModel> res.json());
    }
    creategroup(data):Observable<ResponseModel>{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.post(this.apiUrl+ 'create_group', JSON.stringify(data),{headers: headers})
        .map(res => <ResponseModel> res.json());
    }
    createfriend(data):Observable<ResponseModel>{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.post(this.apiUrl+ 'create_friend', JSON.stringify(data),{headers: headers})
        .map(res => <ResponseModel> res.json());
    }
    addbill(data): Observable<ResponseModel>{
        let headers =new Headers();
        headers.append('Content-Type','application/json');
        return this.http.post(this.apiUrl+ 'add_bill' , JSON.stringify(data), {headers:headers})
        .map(res => <ResponseModel> res.json());
    }
    getbillListing(userID): Observable<ResponseModel>{
        let headers =new Headers();
        headers.append('Content-Type','application/json');
        return this.http.get(this.apiUrl + ''+ userID ,{headers:headers})
        .map(res => <ResponseModel> res.json());
    }
    getgrouplisting(data): Observable<ResponseModel>{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.get(this.apiUrl+ 'get_groups/'+ data.user_id, {headers:headers})
        .map(res => <ResponseModel> res.json());
    }
    getgroupdetail(group_id): Observable<ResponseModel>{
        let headers =new Headers();
        headers.append('Content-Type','application/json');
        return this.http.get(this.apiUrl+ 'get_group_detail/'+ group_id,{headers:headers})
        .map(res => <ResponseModel> res.json());
    }
    deletegroup(group_id): Observable<ResponseModel>{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.get(this.apiUrl+ 'delete_group/'+ group_id, {headers:headers})
        .map(res => <ResponseModel> res.json());
    }
    
    getfriendlisting(userID): Observable<ResponseModel>{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.get(this.apiUrl+ 'friends_list/' + userID, {headers})
        .map(res => <ResponseModel> res.json());
    }
    frienddetail(data): Observable<ResponseModel>{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.get(this.apiUrl + 'single_friend/'+ data.user_id+'/'+ data.friend_id,{headers: headers})
        .map(res => <ResponseModel> res.json());
    }
    deletefriend(data):Observable<ResponseModel>{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.get(this.apiUrl +'delete_friend/'+data.user_id + '/'+ data.friend_id,{headers: headers})
        .map(res => <ResponseModel> res.json());
    }
    deleteUserFromGroup(data) : Observable< ResponseModel>{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.post(this.apiUrl+ 'delete_user_from_group', JSON.stringify(data), {headers:headers})
        .map(res => <ResponseModel> res.json());
    }
    
}